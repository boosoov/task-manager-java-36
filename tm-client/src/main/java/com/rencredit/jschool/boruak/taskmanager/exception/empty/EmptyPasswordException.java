package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyPasswordException extends AbstractClientException {

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }

}
