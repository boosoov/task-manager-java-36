package com.rencredit.jschool.boruak.taskmanager.listener.project;

import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import com.rencredit.jschool.boruak.taskmanager.util.ViewUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProjectShowTaskListListener extends AbstractListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-tasks-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show tasks list by project.";
    }

    @Override
    @EventListener(condition = "@projectShowTaskListListener.name() == #event.command")
    public void handle(final ConsoleEvent event)throws EmptyUserIdException_Exception, DeniedAccessException_Exception, EmptyUserException, EmptyProjectIdException_Exception {
        System.out.println("[LIST PROJECTS]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();

        @NotNull final SessionDTO session = systemObjectService.getSession();
        @NotNull final List<TaskDTO> tasks = projectEndpoint.getTasksByProject(session, projectId);
        @NotNull int index = 1;
        for (@NotNull final TaskDTO task : tasks) {
            System.out.println(index + ". ");
            ViewUtil.showTask(task);
            index++;
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
