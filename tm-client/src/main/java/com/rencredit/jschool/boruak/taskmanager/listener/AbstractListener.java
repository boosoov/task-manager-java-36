package com.rencredit.jschool.boruak.taskmanager.listener;

import com.rencredit.jschool.boruak.taskmanager.api.service.ISystemObjectService;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyProjectException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptySessionException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class AbstractListener {

    @Autowired
    protected ISystemObjectService systemObjectService;

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public Role[] roles() {
        return null;
    }

    public abstract void handle(final ConsoleEvent event) throws DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyNewPasswordException_Exception, IncorrectHashPasswordException_Exception, EmptyHashLineException_Exception, EmptyUserException, NotExistUserException_Exception, EmptyRoleException_Exception, EmptySessionException_Exception, EmptyUserIdException_Exception, EmptyLoginException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyLastNameException_Exception, EmptyEmailException_Exception, EmptyMiddleNameException_Exception, EmptyFirstNameException_Exception, EmptySessionException, EmptyTaskException_Exception, IncorrectIndexException_Exception, EmptyNameException_Exception, EmptyTaskException, EmptyDescriptionException_Exception, IOException_Exception, EmptyProjectException_Exception, EmptyProjectException, EmptyProjectIdException_Exception, EmptyTaskIdException_Exception, EmptyCommandException, EmptyElementsException_Exception, ClassNotFoundException_Exception;

    public AbstractListener() {
    }

}
