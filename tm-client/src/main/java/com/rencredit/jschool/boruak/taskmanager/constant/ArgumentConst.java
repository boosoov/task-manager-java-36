package com.rencredit.jschool.boruak.taskmanager.constant;

import org.jetbrains.annotations.NotNull;

public interface ArgumentConst {

    @NotNull String HELP = "-h";

    @NotNull String VERSION = "-v";

    @NotNull String ABOUT = "-a";

    @NotNull String EXIT = "-e";

    @NotNull String INFO = "-i";

    @NotNull String COMMANDS = "-cmd";

    @NotNull String ARGUMENTS = "-arg";

}
