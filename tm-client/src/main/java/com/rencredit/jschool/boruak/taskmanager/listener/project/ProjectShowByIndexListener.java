package com.rencredit.jschool.boruak.taskmanager.listener.project;

import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyProjectException;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import static com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil.showProject;

@Component
public class ProjectShowByIndexListener extends AbstractListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-view-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by index.";
    }

    @Override
    @EventListener(condition = "@projectShowByIndexListener.name() == #event.command")
    public void handle(final ConsoleEvent event)throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception, EmptyProjectException {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final SessionDTO session = systemObjectService.getSession();
        @Nullable final ProjectDTO project = projectEndpoint.findProjectByIndex(session, index);
        showProject(project);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
