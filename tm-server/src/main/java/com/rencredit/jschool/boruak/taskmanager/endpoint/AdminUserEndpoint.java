package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IAdminUserEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    @NotNull
    @Autowired
    private IUserService userService;

    public AdminUserEndpoint() {
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO lockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws DeniedAccessException, EmptyLoginException, EmptyUserException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        return userService.lockUserByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO unlockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws DeniedAccessException, EmptyLoginException, EmptyUserException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        return userService.unlockUserByLogin(login);
    }

}
